<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateNodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('node', function (Blueprint $table) {
            $table->integer('id');
            $table->integer('parent');
            $table->integer('superparent');
            $table->integer('author');
            $table->string('type');
            $table->string('subtype');
            $table->string('subsubtype');
            $table->timestamp('published')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('created')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('modified')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('notetimestamp')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('version');
            $table->string('slug');
            $table->string('name');
            $table->string('path');
            $table->integer('love');
            $table->integer('games');
            $table->integer('articles');
            $table->integer('posts');
            $table->float('cool');
            $table->float('feedback');
            $table->float('given');
            $table->float('grade');
            $table->float('smart');
            $table->boolean('annonymous');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('node');
    }
}
