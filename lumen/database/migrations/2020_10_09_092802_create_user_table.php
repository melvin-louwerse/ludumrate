<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent');
            $table->integer('superparent');
            $table->integer('author');
            $table->string('type');
            $table->string('subtype');
            $table->timestamp('published')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('created')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('modified')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('version');
            $table->string('slug');
            $table->string('name');
            $table->string('path');
            $table->text('body');
            $table->integer('love');
            $table->integer('games');
            $table->integer('articles');
            $table->integer('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
