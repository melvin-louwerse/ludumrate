start-dev:
	docker-compose up -d

clean-dev:
	docker-compose rm --force php

stop-dev:
	docker-compose stop

rebuild-dev: stop-dev clean-dev
	docker-compose build php

status-dev:
	docker-compose ps

ssh-php:
	PHPHOST=$$(docker ps --format "{{.Names}}" | grep _php_); \
	docker exec -i -t  $${PHPHOST} /bin/zsh;

ssh-web:
	WEBHOST=$$(docker ps --format "{{.Names}}" | grep _web_); \
	docker exec -i -t  $${WEBHOST} /bin/bash;
